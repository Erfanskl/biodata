document.getElementById('biodataForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const name = document.getElementById('name').value.trim(); // Mengambil nilai nama dan menghilangkan spasi di awal/akhir
    const dob = document.getElementById('dob').value; // Mengambil nilai tanggal lahir
    const email = document.getElementById('email').value.trim(); // Mengambil nilai email dan menghilangkan spasi di awal/akhir
    const phone = document.getElementById('phone').value.trim(); // Mengambil nilai nomor HP dan menghilangkan spasi di awal/akhir
    const agama = document.getElementById('agama').value; // Mengambil nilai agama
    const gender = document.querySelector('input[name="gender"]:checked')?.value; // Mengambil nilai jenis kelamin yang dipilih
    const alamat = document.getElementById('alamat').value.trim(); // Mengambil nilai alamat dan menghilangkan spasi di awal/akhir

    // Validasi input
    if (!name || !/^[a-zA-Z\s]+$/.test(name)) {
        alert('Nama harus diisi dengan huruf saja.');
        return;
    }

    if (!dob) {
        alert('Tanggal lahir harus diisi.');
        return;
    }

    if (!email || !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
        alert('Email harus diisi dengan format yang benar.');
        return;
    }

    if (!phone || !/^\d+$/.test(phone)) {
        alert('Nomer HP harus diisi dengan angka saja.');
        return;
    }

    if (!agama) {
        alert('Agama harus dipilih.');
        return;
    }

    if (!gender) {
        alert('Jenis kelamin harus dipilih.');
        return;
    }

    if (!alamat) {
        alert('Alamat harus diisi.');
        return;
    }

    alert(`
        Nama: ${name}
        Tanggal Lahir: ${dob}
        Email: ${email}
        Nomer HP: ${phone}
        Agama: ${agama}
        Jenis Kelamin: ${gender}
        Alamat: ${alamat}
    `); // Menampilkan data yang diinput dalam bentuk alert

    const resultDiv = document.getElementById('result'); // Mengambil elemen untuk menampilkan hasil
    resultDiv.innerHTML = `
        <h3>Hasil Biodata</h3>
        <p><strong>Nama:</strong> ${name}</p>
        <p><strong>Tanggal Lahir:</strong> ${dob}</p>
        <p><strong>Email:</strong> ${email}</p>
        <p><strong>Nomer HP:</strong> ${phone}</p>
        <p><strong>Agama:</strong> ${agama}</p>
        <p><strong>Jenis Kelamin:</strong> ${gender}</p>
        <p><strong>Alamat:</strong> ${alamat}</p>
    `; // Menampilkan hasil input di div dengan id "result"
});
